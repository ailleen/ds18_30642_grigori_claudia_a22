

package com.server.main;

import com.base.IRemote;
import org.apache.log4j.Logger;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

public class RemoteImpl extends UnicastRemoteObject implements IRemote {

    private static final long serialVersionUID = 1569044120491593843L;

    final static Logger log = Logger.getLogger(RemoteImpl.class);

    protected RemoteImpl() throws RemoteException {
        super();
        log.info("Server >> RemoteImpl [ Constructor ]");
    }

    public double computeTax(int engineCapacity) throws RemoteException {
        double result = (engineCapacity / 200 * getSum(engineCapacity));
        log.info("Server >> #computeTax for engine-capacity= " + engineCapacity + " >>> result = " + result);
        return result;
    }

    public double computeSellPrice(int year, double purchasePrice) throws RemoteException {
        double result = computeSellingPrice(year, purchasePrice);
        log.info("Server >> #computeSellPrice: year= " + year + ", purchase-price= " + purchasePrice + " >>> result = " + result);
        return result;
    }

    private int getSum(int engineCapacity) {
        if (engineCapacity < 0) {
            return 0;
        }

        int sum;
        if (engineCapacity <= 1600) {
            sum = 8;
        } else if (engineCapacity <= 2000) {
            sum = 18;
        } else if (engineCapacity <= 2600) {
            sum = 72;
        } else if (engineCapacity <= 3000) {
            sum = 144;
        } else {
            sum = 290;
        }

        return sum;
    }

    private double computeSellingPrice(int year, double purchasePrice) {
//        int yearDiff = 2018 - year;
//        return (purchasePrice - purchasePrice / 7 * yearDiff);
        int yearDifference = 2018 - year;
        if (yearDifference >= 7) {
            return 0;
        }

        double sellingPrice = 0d;
        Calendar calendar = Calendar.getInstance();
        if (purchasePrice < 0) {
            throw new IllegalArgumentException("Purchase price must be positive.");
        } else if (year > calendar.get(Calendar.YEAR)) {
            throw new IllegalArgumentException("Year cannot be greater than current year value.");
        } else {
            sellingPrice = purchasePrice - ((purchasePrice / 7) * yearDifference);
        }

        NumberFormat formatter = new DecimalFormat("###.##");
        String formattedString = formatter.format(sellingPrice);
        System.out.println(" >> " + formattedString);

        return sellingPrice;
    }
}

/*
package com.server.main;

import com.base.IRemote;
import org.apache.log4j.Logger;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class RemoteImpl extends UnicastRemoteObject implements IRemote {

    private static final long serialVersionUID = 1569044120491593843L;

    final static Logger log = Logger.getLogger(RemoteImpl.class);

    protected RemoteImpl() throws RemoteException {
        super();
        log.info("Server >> RemoteImpl [ Constructor ]");
    }

    public double computeTax(int engineCapacity) throws RemoteException {
        double result = (engineCapacity / 200 * getSum(engineCapacity));
        log.info("Server >> #computeTax for engine-capacity= " + engineCapacity + " >>> result = " + result);
        return result;
    }

    public double computeSellPrice(int year, double purchasePrice) throws RemoteException {
        double result = computePrice(year, purchasePrice);
        log.info("Server >> year= " + year + ", purchase-price= " + purchasePrice + " >>> sell-price = " + result);
        return result;
    }

    private int getSum(int engineCapacity) {
//        int sum = 0;
//        if (engineCapacity > 0 && engineCapacity <= 1600) {
//            sum = 8;
//        } else if (engineCapacity <= 2000) {
//            sum = 18;
//        } else if (engineCapacity <= 2600) {
//            sum = 72;
//        } else if (engineCapacity <= 3000) {
//            sum = 144;
//        } else {
//            sum = 290;
//        }
//        return sum;

//        if (engineCapacity <= 0) {
//            throw new IllegalArgumentException("Engine capacity must be positive.");
//        }
        int sum = 8;
        if(engineCapacity > 1601) sum = 18;
        if(engineCapacity > 2001) sum = 72;
        if(engineCapacity > 2601) sum = 144;
        if(engineCapacity > 3001) sum = 290;
        return (int)(engineCapacity / 200.0 * sum);
    }


    private double computePrice(int year, double purchasePrice) {
        //return yearDifference >= 7 ? (purchasePrice - ((purchasePrice * yearDifference) / 7)) : 0;

        int yearDifference = 2018 - year;
        if (yearDifference >= 7) {
            return 0;
        }
        return (purchasePrice * (7 - yearDifference)) / 7; //c.getPurchasePrice() - ((c.getPurchasePrice() / 7) * yearDifference)

//        Calendar calendar = Calendar.getInstance();
//        if (c.getPurchasePrice() < 0) {
//            throw new IllegalArgumentException("Purchase price must be positive.");
//        } else if (c.getYear() > calendar.get(Calendar.YEAR)) {
//            throw new IllegalArgumentException("Year cannot be greater than current year value.");
//        } else {
//        }
    }
}
*/
