
package com.server.main;

import com.base.Constants;
import org.apache.log4j.Logger;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    final static Logger log = Logger.getLogger(Server.class);

    public static void main(String[] args) throws AlreadyBoundException {

        try {
            RemoteImpl impl = new RemoteImpl();
            Registry registry = LocateRegistry.createRegistry(Constants.RMI_PORT);
            registry.bind(Constants.RMI_ID, impl);

            log.info("Server >> Started ...");
        } catch (RemoteException e) {
            log.error("Server >> " + e.getMessage());
        }
    }
}








/*
package com.server.main;

import com.base.Constants;
import com.base.IRemote;
import org.apache.log4j.Logger;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    final static Logger log = Logger.getLogger(Server.class);

    public static void main(String[] args) throws AlreadyBoundException {

        try {

            IRemote server = new RemoteImpl();
            //IRemote stub = (RemoteImpl) UnicastRemoteObject.exportObject((IRemote) server, 0);
            Registry registry = LocateRegistry.createRegistry(Constants.RMI_PORT);
            registry.rebind(Constants.RMI_ID, server);

//            IRemote impl = new RemoteImpl(); // Instantiating the implementation class
//            Registry registry = LocateRegistry.createRegistry(Constants.RMI_PORT);
//            registry.bind(Constants.RMI_ID, impl);

//            IRemote stub = (IRemote) UnicastRemoteObject.exportObject(impl, Constants.RMI_PORT);  // export the remote object to the stub
//            Registry registry2 = LocateRegistry.getRegistry(); // Binding the remote object (stub) in the registry
//            registry2.bind(Constants.RMI_ID, stub);

            log.info("Server >> Started ...");
        } catch (RemoteException e) {
            log.error("Server >> " + e.getMessage());
        }
    }
}*/
