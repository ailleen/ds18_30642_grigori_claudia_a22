package com.client;

import com.base.Constants;
import com.base.IRemote;
import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class MainWindow {

    final static Logger log = Logger.getLogger(MainWindow.class);

    private static IRemote lookupService;
    private static Registry registry;
    protected Shell shell;
    private Text resultTxt;
    private Text purchasePriceTxt;

    /**
     * Launch the application.
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            MainWindow window = new MainWindow();

            initBasic();
            window.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open the window.
     */
    public void open() {
        Display display = Display.getDefault();
        createContents();

        // shell.pack();
        shell.open();
        shell.layout();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
    }

    /**
     * Create contents of the window.
     */
    protected void createContents() {
        shell = new Shell();
        shell.setSize(600, 400);
        shell.setText("JAVA RMI - 30642");
        shell.setLayout(new GridLayout(11, false));

        for (int i = 0; i <= 17; i++) {
            new Label(shell, SWT.NONE);
        }

        Label lblNewLabel = new Label(shell, SWT.NONE);
        lblNewLabel.setText("Engine Capacity");
        new Label(shell, SWT.NONE);

        final Spinner spinnerCapacity = new Spinner(shell, SWT.BORDER);
        spinnerCapacity.setMinimum(1000);
        spinnerCapacity.setMaximum(3000);
        for (int i = 0; i <= 5; i++) {
            new Label(shell, SWT.NONE);
        }

        Button computeTaxBtn = new Button(shell, SWT.NONE);
        computeTaxBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent arg0) {
                int engineCapacityValue = Integer.valueOf(spinnerCapacity.getText());

                log.info("ENGINE CAPACITY == " + engineCapacityValue);

                try {
                    // clearResults();

                    double computedTax = lookupService.computeTax(engineCapacityValue);
                    resultTxt.setText(String.valueOf(computedTax));
                    // clearResults();
                } catch (RemoteException e) {
                    log.error(e.getMessage());
                    // e.printStackTrace();
                }
//				log.info("Client Window - TAX VALUE");
            }
        });
        computeTaxBtn.setText("Compute TAX");
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);

        Label lblNewLabel_1 = new Label(shell, SWT.NONE);
        lblNewLabel_1.setText("Fabrication Year");
        new Label(shell, SWT.NONE);

        final Spinner spinnerFabricationYear = new Spinner(shell, SWT.BORDER);
        spinnerFabricationYear.setMinimum(1970);
        spinnerFabricationYear.setMaximum(2018);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);

        Label lblNewLabel_2 = new Label(shell, SWT.NONE);
        lblNewLabel_2.setText("Purchase Price");
        new Label(shell, SWT.NONE);

        purchasePriceTxt = new Text(shell, SWT.BORDER);
        purchasePriceTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);

        Button computeSellPriceBtn = new Button(shell, SWT.NONE);
        computeSellPriceBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent arg0) {

                //clearResults();
                try {
                    double purchaseValue = Double.parseDouble(purchasePriceTxt.getText());
                    int fabricationYear = Integer.valueOf(spinnerFabricationYear.getText());

                    double computedPrice = lookupService.computeSellPrice(fabricationYear, purchaseValue);
                    // resultTxt.setText(String.valueOf(computedPrice));
                    resultTxt.setText(computedPrice + "");
                } catch (NumberFormatException e) {
                    log.error(e.getMessage());
                    resultTxt.setText("Valoarea introdusa nu este valida!");
                } catch (RemoteException e) {
                    log.error(e.getMessage());
                }
            }
        });

        computeSellPriceBtn.setText("Find SELL PRICE");
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);

        Label lblNewLabel_3 = new Label(shell, SWT.NONE);
        lblNewLabel_3.setText("Result");
        new Label(shell, SWT.NONE);

        resultTxt = new Text(shell, SWT.BORDER);
        resultTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);
        new Label(shell, SWT.NONE);

        /*
         * handleComputeTax(); handleComputeSellPrice();
         */
    }

    public static void initBasic() {
        try {
            registry = LocateRegistry.getRegistry("localhost", Constants.RMI_PORT);
            lookupService = (IRemote) registry.lookup(Constants.RMI_ID);

            log.info("MainWindow >> Started... ");
            log.info(" 		> registry port = " + Registry.REGISTRY_PORT);
            log.info(" 		> lookup = " + lookupService.toString());
        } catch (RemoteException e) {
            log.error(" RemoteException >> \n" + e.getCause());
            e.printStackTrace();
        } catch (NotBoundException e) {
            log.error(" NotBoundException >> \n" + e.getMessage());
            e.printStackTrace();
        } catch (Exception e){
            log.error(" Exception >> \n" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void clearResults() {
        resultTxt.setText("");
        purchasePriceTxt.setText("");
    }
}