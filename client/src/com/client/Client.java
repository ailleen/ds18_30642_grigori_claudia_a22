package com.client;

import com.base.Constants;
import com.base.IRemote;
import org.apache.log4j.Logger;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class Client {

    final static Logger log = Logger.getLogger(Client.class);

    public static void main(String[] args) {

        Registry registry;
        try {
            registry = LocateRegistry.getRegistry("localhost", Constants.RMI_PORT);
            IRemote lookup = (IRemote) registry.lookup(Constants.RMI_ID);

            log.info("Client >> Started... ");
            //log.info(" 		> client.registry port = " + Registry.REGISTRY_PORT);
            log.info(" 		> client.lookup = " + lookup.toString());

        } catch (RemoteException e) {
            log.error("Client RemoteException >> \n" + e.getCause());
            e.printStackTrace();
        } catch (NotBoundException e) {
            log.error("Client NotBoundException >> \n" + e.getMessage());
        } catch (Exception e){
            log.error(" Exception >> \n" + e.getMessage());
            e.printStackTrace();
        }

    }

}
