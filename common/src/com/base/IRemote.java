package com.base;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IRemote extends Remote, Serializable {

    double computeTax(int engineCapacity) throws RemoteException;

    double computeSellPrice(int year, double purchasePrice) throws RemoteException;

}
